﻿using Abp.AutoMapper;
using RIS.Dto;
using RIS.EntitiesDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIS.Dto
{
    [AutoMap(typeof(ObjectDetail))]
    public class BaiVietInputDto : ObjectDetail
    {
        public List<FileUploadInputDto> FileUploadtInputDtos { get; set; }
    }

    public class BaiVietDto : PagedAndFilteredInputDto
    {
        public long? MenuId { get; set; }
        public string Title { get; set; }
    }
    public class BaiVietOutputDto : ObjectDetail
    {
        public List<FileUploadOutputDto> FileUploadtOutputDtos { get; set; }
    }
}
