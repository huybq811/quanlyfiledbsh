﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization.Users;
using Abp.Domain.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using Abp.UI;
using RIS.Authorization.Roles;
using RIS.MultiTenancy;
using System.Net.Mail;
using Microsoft.Extensions.Configuration;
using Abp.Linq;

namespace RIS.Authorization.Users
{
    public class UserRegistrationManager : DomainService
    {
        public IAbpSession AbpSession { get; set; }
        public IAsyncQueryableExecuter AsyncQueryableExecuter { get; set; }

        private readonly TenantManager _tenantManager;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IConfiguration _iConfiguration;

        public UserRegistrationManager(
            TenantManager tenantManager,
            UserManager userManager,
            RoleManager roleManager,
            IPasswordHasher<User> passwordHasher,
            IConfiguration configuration)
        {
            _tenantManager = tenantManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _passwordHasher = passwordHasher;
            _iConfiguration = configuration;

            AbpSession = NullAbpSession.Instance;
            AsyncQueryableExecuter = NullAsyncQueryableExecuter.Instance;
        }

        public async Task<User> RegisterAsync(string name, string surname, string emailAddress, string userName, string phoneNumber, string plainPassword, bool isEmailConfirmed)
        {
            try
            {


                var user = new User
                {
                    TenantId = null,
                    Name = name,
                    Surname = surname,
                    EmailAddress = emailAddress,
                    PhoneNumber = phoneNumber,
                    IsActive = false,
                    UserName = userName,
                    IsEmailConfirmed = isEmailConfirmed,
                    Roles = new List<UserRole>()
                };

                user.SetNormalizedNames();

                var defaultRoles = await AsyncQueryableExecuter.ToListAsync(_roleManager.Roles.Where(r => r.IsDefault));
                foreach (var defaultRole in defaultRoles)

                {
                    user.Roles.Add(new UserRole(null, user.Id, defaultRole.Id));
                }

                //await _userManager.InitializeOptionsAsync(tenant.Id);

                CheckErrors(await _userManager.CreateAsync(user, plainPassword));
                await CurrentUnitOfWork.SaveChangesAsync();
                if (!user.IsEmailConfirmed)
                {
                    //send mail here
                    SendmailRigisterAccount(user.EmailAddress);
                }

                return user;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void SendmailRigisterAccount(string account)
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(_iConfiguration.GetValue<string>("Smtp:FromAddress", "defaultfromaddress"), _iConfiguration.GetValue<string>("Smtp:Password", "defaultfrompassword"));
            //client.Credentials = new System.Net.NetworkCredential("quanlyantoandap@gmail.com", "yqhd nnxn jght rxjx");
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
            var toaddress = _iConfiguration.GetValue<string>("Smtp:ToAddress", "httldbsh@gmail.com");
            if (!string.IsNullOrEmpty(toaddress))
            {
                foreach (var address in toaddress.Split(";"))
                {
                    mailMessage.To.Add(address.Trim());
                }
            }
            mailMessage.From = new MailAddress(_iConfiguration.GetValue<string>("Smtp:FromAddress", "defaultfromaddress"));
            mailMessage.Body = "Tài khoản" + account + " đã đăng ký thành công. Vui lòng truy cập: http://httldbsh.com/users để duyệt tài khoản";
            mailMessage.Subject = "Xác nhận tài khoản";
            mailMessage.IsBodyHtml = true;
            client.Send(mailMessage);
        }

        private void CheckForTenant()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                throw new InvalidOperationException("Can not register host users!");
            }
        }

        private async Task<Tenant> GetActiveTenantAsync()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                return null;
            }

            return await GetActiveTenantAsync(AbpSession.TenantId.Value);
        }

        private async Task<Tenant> GetActiveTenantAsync(int tenantId)
        {
            var tenant = await _tenantManager.FindByIdAsync(tenantId);
            if (tenant == null)
            {
                throw new UserFriendlyException(L("UnknownTenantId{0}", tenantId));
            }

            if (!tenant.IsActive)
            {
                throw new UserFriendlyException(L("TenantIdIsNotActive{0}", tenantId));
            }

            return tenant;
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
