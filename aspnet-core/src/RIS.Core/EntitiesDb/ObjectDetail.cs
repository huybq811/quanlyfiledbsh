﻿using Abp.Domain.Entities.Auditing;
using Castle.MicroKernel.SubSystems.Conversion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIS.EntitiesDb
{

    [Table("ObjectDetail")]
    public class ObjectDetail : FullAuditedEntity<long>
    {
        public string ObjectDetailId {get; set;}
        public long MenuId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public Boolean Activated { get; set; } = true;  

    }
}
