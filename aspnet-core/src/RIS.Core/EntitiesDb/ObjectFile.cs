﻿using Abp.Domain.Entities.Auditing;
using Castle.MicroKernel.SubSystems.Conversion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIS.EntitiesDb
{

    [Table("ObjectFile")]
    public class ObjectFile : FullAuditedEntity<long>
    {
        public string ObjectDetailId { get; set; }
        public string Name { get; set; }
        [StringLength(2000)]
        public string FileName { get; set; }
        [StringLength(2000)]
        public string Url { get; set; }
        [StringLength(500)]
        public string FormatFile { get; set; }
        public int Type { get; set; }
        public bool IsDownload { get; set; }
        public Boolean Activated { get; set; } = true;
    }
}
