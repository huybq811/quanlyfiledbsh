﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Ris.ApplicationDto;
using RIS.Dto;
using RIS.EntitiesDb;
using RIS.RisAppCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RIS.App
{
    [AbpAuthorize]
    public class BaiVietAppService : RISAppServiceBase, IBaiVietAppService
    {
        private readonly IRepository<ObjectDetail, long> _objectDetailRepos;
        private readonly IRepository<ObjectFile, long> _objectFileRepos;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public BaiVietAppService(
            IRepository<ObjectDetail, long> objectDetailRepos,
            IRepository<ObjectFile, long> objectFileRepos,
            IHttpContextAccessor httpContextAccessor
            )
        {
            _objectDetailRepos = objectDetailRepos;
            _objectFileRepos = objectFileRepos;
            _httpContextAccessor = httpContextAccessor;
        }
        [AbpAuthorize]
        public async Task<CommonResponseDto> GetAll(BaiVietDto input)
        {
            CommonResponseDto commonResponseDto = new CommonResponseDto();
            try
            {
                var query = (from detail in _objectDetailRepos.GetAll()
                             where detail.MenuId == input.MenuId
                             select new BaiVietOutputDto
                             {
                                 Id = detail.Id,
                                 MenuId = detail.MenuId,
                                 ObjectDetailId = detail.ObjectDetailId,
                                 Title = detail.Title,
                                 Content = detail.Content,
                                 Description = detail.Description,
                                 CreationTime = detail.CreationTime,
                                 FileUploadtOutputDtos = 
                                 (from file in _objectFileRepos.GetAll()
                                 where file.ObjectDetailId == detail.ObjectDetailId
                                 select new FileUploadOutputDto
                                 { 
                                     Id = file.Id,
                                     Name = file.Name,
                                     FileName = file.FileName,
                                     Url = file.Url,
                                     FormatFile = file.FormatFile,
                                     ViewUrl = _httpContextAccessor.HttpContext.Request.Scheme + "://" + _httpContextAccessor.HttpContext.Request.Host.Value + file.Url.Replace("wwwroot", ""),
                                     Type = file.Type
                                 }).ToList()
                             })
                             .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), x => x.Title.ToLower().Contains(input.Filter.ToLower()));
                var lstObj = await query.Skip(input.SkipCount).Take(input.MaxResultCount).OrderBy(x => x.CreationTime).ToListAsync();
                var totalCout = await query.CountAsync();
                PagedResultDto<BaiVietOutputDto> pagedResultDto = new PagedResultDto<BaiVietOutputDto>();
                pagedResultDto.TotalCount = totalCout;
                pagedResultDto.Items = lstObj;
                commonResponseDto.ReturnValue = pagedResultDto;
                commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThanhCong;
                commonResponseDto.Message = "Thành Công";
            }
            catch (Exception ex)
            {
                commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThatBai;
                commonResponseDto.Message = ex.Message;
                throw;
            }
            return commonResponseDto;
        }

        [AbpAuthorize]
        public async Task<CommonResponseDto> CreateOrUpdate(BaiVietInputDto input)
        {
            CommonResponseDto commonResponseDto = new CommonResponseDto();
            try
            {
                var _obj = await _objectDetailRepos.FirstOrDefaultAsync(x => x.ObjectDetailId == input.ObjectDetailId);
                if (_obj != null)
                {
                    _obj.Title = input.Title;
                    _obj.Content = input.Content;
                    _obj.MenuId = input.MenuId;
                    _obj.Description = input.Description;
                    await _objectDetailRepos.UpdateAsync(_obj);
                }
                else
                {
                    var _objBaiViet = input.MapTo<BaiVietInputDto>();
                    await _objectDetailRepos.InsertAsync(_objBaiViet);
                }
                commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThanhCong;
                commonResponseDto.Message = "Thành Công";
            }
            catch (Exception ex)
            {
                commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThatBai;
                commonResponseDto.Message = ex.Message;
                throw;
            }
            return commonResponseDto;
        } 

        [AbpAuthorize]
        public async Task<CommonResponseDto> GetById(string objectDetailId)
        {
            CommonResponseDto commonResponseDto = new CommonResponseDto();
            try
            {
                var _objDetail = await _objectDetailRepos.FirstOrDefaultAsync(x => x.ObjectDetailId == objectDetailId);
                var lstFile = ( from file in _objectFileRepos.GetAll()
                                where file.ObjectDetailId == objectDetailId
                                select new FileUploadOutputDto
                                {
                                    Id = file.Id,
                                    ObjectDetailId = file.ObjectDetailId,
                                    Name = file.Name,
                                    FileName = file.FileName,
                                    Url = file.Url,
                                    ViewUrl = _httpContextAccessor.HttpContext.Request.Scheme + "://" + _httpContextAccessor.HttpContext.Request.Host.Value + file.Url.Replace("wwwroot", ""),
                                    FormatFile = file.FormatFile,
                                    Type = file.Type,
                                    IsDownload = file.IsDownload
                                }).ToList();
                var obj = new BaiVietOutputDto()
                {
                    Id = _objDetail.Id,
                    MenuId = _objDetail.MenuId,
                    ObjectDetailId = _objDetail.ObjectDetailId,
                    Title = _objDetail.Title,
                    Content = _objDetail.Content,
                    Description = _objDetail.Description,
                    FileUploadtOutputDtos = lstFile
                };
                commonResponseDto.ReturnValue = obj;
                commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThanhCong;
                commonResponseDto.Message = "Thành Công";
            }
            catch (Exception ex)
            {
                commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThatBai;
                commonResponseDto.Message = ex.Message;
                throw;
            }
            return commonResponseDto;
        }

        [AbpAuthorize]
        public async Task<CommonResponseDto> Delete(string objectDetailId)
        {
            CommonResponseDto commonResponseDto = new CommonResponseDto();
            try
            {
                var _obj = await _objectDetailRepos.FirstOrDefaultAsync(x => x.ObjectDetailId == objectDetailId);
                if (_obj != null)
                {
                    await _objectDetailRepos.DeleteAsync(_obj);
                    commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThanhCong;
                    commonResponseDto.Message = "Thành Công";
                }
                else
                {
                    commonResponseDto.Message = "Bài viết này không tồn tại";
                    commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThatBai;
                    return commonResponseDto;
                }
            }
            catch (Exception ex)
            {
                commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThatBai;
                commonResponseDto.Message = ex.Message;
                throw;
            }
            return commonResponseDto;
        }
        private async Task<string> WriteFile(byte[] fileUpload, long systemId, string objectDetailId, long objectCategoryId, string name, string type)
        {
            string exactPathDirectory = "";
            try
            {
                //string fileName = name.IsNullOrEmpty() ? DateTime.Now.Ticks.ToString() + "." + type : RemoveVietNamese(name) + DateTime.Now.Ticks.ToString() + "." + type;
                string fileName = DateTime.Now.Ticks.ToString() + "." + type;

                var filePath = "wwwroot\\Uploads\\" + systemId + "\\" + objectDetailId + "\\" + objectCategoryId;
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                exactPathDirectory = "wwwroot\\Uploads\\" + systemId + "\\" + objectDetailId + "\\" + objectCategoryId + "\\" + fileName;
                using (var fs = new FileStream(exactPathDirectory, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(fileUpload, 0, fileUpload.Length);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return exactPathDirectory;
        }
        [AbpAuthorize]
        public async Task<CommonResponseDto> WriteFileFormFile(IFormFile fileUpload, string objectDetailId, string name, bool isdownload)
        {
            CommonResponseDto commonResponseDto = new CommonResponseDto();
            try
            {
                var extension = fileUpload.FileName.Split('.')[fileUpload.FileName.Split('.').Length - 1];
                if (TypeFile(extension) != -1)
                {
                    string fileNameServer = DateTime.Now.Ticks.ToString() + "." + extension;
                    var filePath = "wwwroot\\Uploads\\" + objectDetailId;
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    var exactPath = filePath + "\\" + fileNameServer;
                    using (var stream = new FileStream(exactPath, FileMode.Create))
                    {
                        await fileUpload.CopyToAsync(stream);
                    }
                    var _objFileUploadInput = new FileUploadOutputDto
                    {
                        ObjectDetailId = objectDetailId,
                        Name = name,
                        FileName = fileUpload.FileName,
                        Url = exactPath,
                        FormatFile = extension,
                        Type = TypeFile(extension),
                        IsDownload = isdownload
                    };
                    await _objectFileRepos.InsertAsync(_objFileUploadInput);
                    commonResponseDto.ReturnValue = _objFileUploadInput;
                    commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThanhCong;
                    commonResponseDto.Message = "Thành Công";
                }
                else 
                {
                    commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThatBai;
                    commonResponseDto.Message = "File không đúng định dạng";
                }
            }
            catch (Exception ex)
            {
                commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThatBai;
                commonResponseDto.Message = ex.Message;
                throw;
            }
            return commonResponseDto;
        }
        public FileStreamResult DownloadFile(long fileId)
        {
            var objFile = _objectFileRepos.FirstOrDefault(x => x.Id == fileId);
            var filePath = objFile.Url;
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(filePath, out var contentType))
            {
                contentType = "application/octet-stream";
            }
            return new FileStreamResult(new FileStream(filePath, FileMode.Open), contentType)
            {
                FileDownloadName = objFile.FileName
            };
        }
        [AbpAuthorize]
        public async Task<CommonResponseDto> DeleteFile(long fileId)
        {
            CommonResponseDto commonResponseDto = new CommonResponseDto();
            try
            {
                var _obj = await _objectFileRepos.FirstOrDefaultAsync(x => x.Id == fileId);
                if (_obj != null)
                {
                    await _objectFileRepos.DeleteAsync(_obj);
                    commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThanhCong;
                    commonResponseDto.Message = "Thành Công";
                }
                else
                {
                    commonResponseDto.Message = "File này không tồn tại";
                    commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThatBai;
                    return commonResponseDto;
                }
            }
            catch (Exception ex)
            {
                commonResponseDto.Code = CommonEnum.ResponseCodeStatus.ThatBai;
                commonResponseDto.Message = ex.Message;
                throw;
            }
            return commonResponseDto;
        }
        private static string RemoveVietNamese(string s)
        {
            if (s.IsNullOrEmpty()) return "";
            const string FindText = "áàảãạâấầẩẫậăắằẳẵặđéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵÁÀẢÃẠÂẤẦẨẪẬĂẮẰẲẴẶĐÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴ";
            const string ReplText = "aaaaaaaaaaaaaaaaadeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAADEEEEEEEEEEEIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYY";

            int index = -1;
            while ((index = s.IndexOfAny(FindText.ToCharArray())) != -1)
            {
                int index2 = FindText.IndexOf(s[index]);
                s = s.Replace(s[index], ReplText[index2]);
            }
            return s;
        }
        private static int TypeFile(string s)
        {
            if (s.IsNullOrEmpty()) return -1;
            const string FindDocument = "DOC|DOCX|XLS|XLSX|PDF";
            const string FindMap = "DWG|ARCGIS";
            const string FindPicture = "PNG|JPG|JPEG|GIF|CSV";
            const string FindVideo = "MP4|WEBM|MOV|AVI|MKV|WMV|AVCHD|FLV";

            int index = -1;
            while ((index = s.ToUpper().IndexOfAny(FindDocument.ToCharArray())) != -1)
            {
                return 1;
            }
            while ((index = s.ToUpper().IndexOfAny(FindMap.ToCharArray())) != -1)
            {
                return 2;
            }
            while ((index = s.ToUpper().IndexOfAny(FindPicture.ToCharArray())) != -1)
            {
                return 3;
            }
            while ((index = s.ToUpper().IndexOfAny(FindVideo.ToCharArray())) != -1)
            {
                return 4;
            }
            return -1;
        }
    }
}
