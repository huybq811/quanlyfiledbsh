import React from 'react'

function THeader(props: any) {
    return (
        <div style={{ fontWeight: 'bold' }}>{props.children}</div>
    )
}

export default THeader