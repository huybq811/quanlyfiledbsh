import { Card } from 'antd';
import * as React from 'react';
import PageContent from '../../components/PageContent';
import { URL_PATH } from '../../components/Router/router.path';


export class About extends React.Component<any> {
  render() {
    return (
      <PageContent
        breadcrumb={[
          { title: "Trang chủ", href: URL_PATH.Home },
          { title: "Giới thiệu" },
        ]}>
        <Card size='small' className='px-12'>
          ĐÂY là giới thiệu
        </Card>

      </PageContent>
    );
  }
}
export default About;
