import { PlusOutlined, SettingOutlined } from '@ant-design/icons';
import { Button, Card, Col, Dropdown, Input, Menu, Modal, Row, Table } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React from 'react'
import AppComponentBase from '../../components/AppComponentBase'
import PageContent from '../../components/PageContent';
import { URL_PATH } from '../../components/Router/router.path';
import { ISystemProps, ISystemState } from './models'
import { inject, observer } from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import { ColumnsType } from 'antd/lib/table';
import THeader from '../../components/Tables/tableHeader';
import CreateOrUpdateSystemModal from './components/createOrUpdateSystemModal';
const Search = Input.Search;
const confirm = Modal.confirm;

@inject(Stores.SystemStore, Stores.SessionStore)
@observer
class SystemPage extends AppComponentBase<ISystemProps, ISystemState> {

  formRef = React.createRef<FormInstance>();

  state = {
    modalVisible: false,
    maxResultCount: 10,
    skipCount: 0,
    userId: 0,
    filter: '',
    loading: false
  };

  async componentDidMount() {
    await this.getAll();
  }

  async getAll() {
    this.setState({ loading: true })
    await this.props.systemStore.getAll({
      maxResultCount: this.state.maxResultCount,
      skipCount: this.state.skipCount,
      filter: this.state.filter,
    });
    this.setState({ loading: false })
  }
  handleSearch = (value: string) => {
    this.setState({ filter: value }, async () => await this.getAll());
  };
  createOrUpdateModalOpen = (entity?: any) => {
    this.Modal();
    if (entity) {
      setTimeout(() => {
        this.formRef.current?.setFieldsValue({ ...entity });
      }, 100);
    }
  }
  Modal = () => {
    this.setState({
      modalVisible: !this.state.modalVisible,
    });
  };
  handleTableChange = (pagination: any) => {
    this.setState({ skipCount: (pagination.current - 1) * this.state.maxResultCount! }, async () => await this.getAll());
  };
  handleCreate = () => {
    const form = this.formRef.current;
    form!.validateFields().then(async (values: any) => {
      this.setState({ loading: true })
      await this.props.systemStore.createOreUpdate(values);
      this.setState({ modalVisible: false });
      await this.getAll();
      form!.resetFields();
      this.setState({ loading: false })
    });
  };

  delete(id: any) {
    const self = this;
    confirm({
      title: 'Bạn muốn xóa bản ghi này?',
      onOk() {
        self.props.systemStore.delete({ id }).finally(() => {
          self.getAll()
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }


  render() {
    const { systemList } = this.props.systemStore;
    const columns: ColumnsType<any> = [
      { title: <THeader>Tên hệ thống</THeader>, dataIndex: 'name', key: 'name' },
      { title: <THeader>Mô tả</THeader>, dataIndex: 'description', key: 'description' },
      {
        title: <THeader>Hành động</THeader>,
        dataIndex: 'action',
        width: 120,
        key: 'action',
        render: (_, record) => {
          return (
            <Dropdown
              overlay={
                <Menu>
                  <Menu.Item
                    onClick={() => { this.createOrUpdateModalOpen(record) }}>Chỉnh sửa hệ thống</Menu.Item>
                  <Menu.Item
                    onClick={() => { this.delete(record.id) }}>Xóa hệ thống</Menu.Item>
                </Menu>
              } trigger={['click']}>
              <Button icon={<SettingOutlined />} type='primary' />
            </Dropdown>
          )
        }
      },
    ];

    return (
      <PageContent
        breadcrumb={[
          { title: "Trang chủ", href: URL_PATH.Home },
          { title: "Hệ thống" },
        ]}>

        <Card size='small' className='px-12'>
          <Row justify='space-between' align='middle' gutter={12}>
            <Col >
              <h2 className='mb-0'>Danh sách hệ thống</h2>
            </Col>
            <Row gutter={12}>

              <Col>
                <Search placeholder={"Tìm kiếm"} onSearch={this.handleSearch} />
              </Col>
              <Col >
                <Button
                  type='primary' icon={<PlusOutlined />} onClick={this.createOrUpdateModalOpen}>Tạo mới</Button>
              </Col>
            </Row>
          </Row>
        </Card>
        <Card className='mt-12'>
          <Table
            rowKey={(record) => record.id.toString()}
            bordered={true}
            columns={columns}
            pagination={{ pageSize: 10, total: systemList === undefined ? 0 : systemList.totalCount, defaultCurrent: 1 }}
            loading={this.state.loading}
            dataSource={systemList === undefined ? [] : systemList.items}
            onChange={this.handleTableChange}
            scroll={{ x: 500 }}
          />
        </Card>

        <CreateOrUpdateSystemModal
          title='Danh mục hệ thống'
          formRef={this.formRef}
          visible={this.state.modalVisible}
          confirmLoading={this.state.loading}
          onCancel={() => {
            this.setState({
              modalVisible: false,
            });
            this.formRef.current?.resetFields();
          }}
          onCreate={this.handleCreate}
        />

      </PageContent>
    )
  }
}

export default SystemPage